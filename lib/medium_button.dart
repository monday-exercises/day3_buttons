import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MediumButton extends StatelessWidget {
  final String _text;
  final Color _color;
  final Function _handler;

  MediumButton(this._text, this._color, this._handler);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      margin: EdgeInsets.all(10),
      child: RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0), side: BorderSide(color: Colors.grey)),
        child: Text(
          _text,
          style: TextStyle(fontSize: 30),
        ),
        color: _color,
        onPressed: _handler,
      ),
    );
  }
}
